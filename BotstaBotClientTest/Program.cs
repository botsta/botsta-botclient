﻿using System;
using System.Threading.Tasks;
using Botsta.BotClient;
using Botsta.BotClient.Dto;

namespace Botsta.BotClientTest
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var client = new BotstaBotClient("Heyho", "iJRrz/jm8x", "http://localhost:5000/graphql");
               var ret = (await client.SubscribeMessageStreamAsync()).Subscribe(async (res) => {
                if (res.Data != null)
                {
                    bool isSenderMe = await client.IsSenderMeAsync(res.Data.Message);
                    if (!isSenderMe) // answer only when sender is not me
                    {
                        var builder = new MessageBuilder()
                            .AddTitle("Hallo")
                            .AddSubtitle("Ich bin Olaf! 🤪")
                            .AddPostbackButton("Impress me", "Are you impressed 🎉")
                            .AddUrlButton("Open Website", "https://bauer-jakob.de");
                        //await builder.SubmittMessageAsync(client, res.Data);
                    }
                }
            });

            Console.WriteLine("Press enter to stop!");
            Console.ReadLine();
        }
    }
}
