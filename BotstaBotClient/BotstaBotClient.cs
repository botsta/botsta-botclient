﻿using System;
using System.Net;
using System.Net.Http.Headers;
using System.Net.WebSockets;
using System.Threading.Tasks;
using Botsta.BotClient.Dto;
using GraphQL;
using GraphQL.Client.Abstractions.Websocket;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using System.Security.Cryptography;
using System.IdentityModel.Tokens.Jwt;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;

namespace Botsta.BotClient
{
    public class BotstaBotClient : IDisposable
    {
        private readonly string _botName;
        private readonly string _apiKey;
        private readonly string _endpoint;
        private Guid? _botId;

        private GraphQLHttpClient _client;
        private string _refreshToken;
        private string _token;
        private readonly E2EEService _e2eeService;

        public BotstaBotClient(string botName, string apiKey, string endpoint)
        {
            _endpoint = endpoint;
            _client = new GraphQLHttpClient(endpoint, new NewtonsoftJsonSerializer());
            _botName = botName;
            _apiKey = apiKey;
            _e2eeService = new E2EEService();
        }

        public async Task<GraphQLResponse<MessageResponse>> SubmittMessageAsync(Guid chatroomId, params MessagePart[] messages)
        {
            return await SubmittMessageAsync(chatroomId, messages.ToList());
        }

        public async Task<GraphQLResponse<MessageResponse>> SubmittMessageAsync(Guid chatroomId, IEnumerable<MessagePart> messages)
        {
            var requestString = JsonConvert.SerializeObject(messages);
            return await SubmittMessageAsync(chatroomId, requestString);
        }

        public async Task<GraphQLResponse<MessageResponse>> SubmittMessageAsync(Guid chatroomId, string message)
        {
            var client = await GetGraphQlClientAsync();
            var request = new GraphQLRequest
            {
                Query = @"
                mutation PostMessage($chatroomId: String!, $message: String!) {
                    postMessage(chatroomId: $chatroomId, message:$message) {
                    id
                    sender {
                       id
                       name
                       isBot
                    }
                  }
                }
                ",
                Variables = new
                {
                    chatroomId = chatroomId.ToString(),
                    message = message
                }
            };

            var result = await client.SendQueryAsync<MessageResponse>(request);
            return result;
        }

        public async Task<bool> IsMeAsync(Guid practicantId)
        {
            if (_botId == null)
            {
                var response = await WhoAmIAsync();
                _botId = response.Id;
            }

            return _botId == practicantId;
        }


        public async Task<bool> IsSenderMeAsync(Message message)
        {
            return await IsSenderMeAsync(message.Sender);
        }

        public async Task<bool> IsSenderMeAsync(Sender sender)
        {
            return await IsMeAsync(sender.Id);
        }

        public async Task<IObservable<GraphQLResponse<MessageResponse>>> SubscribeMessageStreamAsync()
        {
            var client = await GetGraphQlClientAsync();
            var request = new GraphQLRequest
            {
                Query = @"
                    subscription MessageSubscription($refreshToken: String!) {
                      messageReceived(refreshToken: $refreshToken) {
                        sender {
                          id
                          name
                          isBot
                        }
                        message
                        senderPublicKey
  	                    chatroomId
  	                    id
                        sendTime
                      }
                    }", Variables = new {
                    refreshToken = _refreshToken
                }
                
            };
            return client.CreateSubscriptionStream<MessageResponse>(request).Select(r =>
            {
                var message = r?.Data?.Message;
                if (message != null)
                {
                    var decryptedMessage = _e2eeService.DecryptMessage(message.EncryptedMessage,
                        Encoding.UTF8.GetBytes(message.SenderPublicKey));
                    message.Text = decryptedMessage;
                }
                return r;
            });
        }

        private async Task<GraphQLHttpClient> GetGraphQlClientAsync()
        {
            if (_refreshToken == null)
            {
                var loginResponse = await LoginAsync();
                _refreshToken = loginResponse.RefreshToken;
                _token = loginResponse.Token;
            }
            else
            {
                var refreshTokenRespone = await RefreshToken();
                _token = refreshTokenRespone.Token;
            }

            _client.HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);
            return _client;
        }

        private async Task<LoginResponse> LoginAsync()
        {
            var request = new GraphQLRequest
            {
                Query = @"
                mutation Login($name: String!, $secret: String!, $publicKey: String!) { 
                    login(name: $name, secret: $secret, publicKey: $publicKey) {
                        token,
                        refreshToken,
                        hasError,
                        errorCode,
                        errorMessage
                    }
                }
                ",
                Variables = new
                {
                    name = _botName,
                    secret = _apiKey,
                    publicKey = _e2eeService.PublicKeyHex
                }
            };

            var result = await _client.SendQueryAsync<JObject>(request);
            var ret = JsonConvert.DeserializeObject<LoginResponse>(result.Data.GetValue("login").ToString());
            return ret;
        }

        private async Task<WhoAmIResponse> WhoAmIAsync()
        {
            var request = new GraphQLRequest
            {
                Query = @"
                query WhoAmI { 
                    whoami {
                        id,
                        name,
                        isUser
                    }
                }
                "
            };

            var result = await _client.SendQueryAsync<JObject>(request);
            var ret = JsonConvert.DeserializeObject<WhoAmIResponse>(result.Data.GetValue("whoami").ToString());
            return ret;
        }

        private async Task<RefreshTokenResponse> RefreshToken()
        {
            var request = new GraphQLRequest
            {
                Query = @"
                    mutation RefresthToken {
                      refreshToken {
                        token,
                        hasError,
                        errorCode,
                        errorMessage
                      }
                    }
                "
            };

            _client.HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _refreshToken);

            var result = await _client.SendQueryAsync<JObject>(request);
            var ret = JsonConvert.DeserializeObject<RefreshTokenResponse>(result.Data.GetValue("refreshToken").ToString());
            return ret;
        }

        private void GenerateKeyPair()
        {
           
            //var pubKey = _e2eeKey.PublicKey.ToByteArray();
        }

        public void Dispose()
        {
            _client.Dispose();
            _e2eeService.Dispose();
        }
    }
}
