﻿using System;
using Newtonsoft.Json;

namespace Botsta.BotClient.Dto
{
    public class WhoAmIResponse
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
