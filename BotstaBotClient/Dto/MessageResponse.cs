﻿using System;
using Newtonsoft.Json;

namespace Botsta.BotClient.Dto
{
    public class MessageResponse
    {
        [JsonProperty("messageReceived")]
        public Message Message { get; set; }
    }

    public class Message
    {
        [JsonProperty("chatroomId")]
        public Guid ChatroomId { get; set; }

        [JsonProperty("message")]
        internal string EncryptedMessage { get; set; }

        public string Text { get; set; }

        [JsonProperty("sender")]
        public Sender Sender { get; set; }

        [JsonProperty("senderPublicKey")]
        internal string SenderPublicKey { get; set; }

        [JsonProperty("sendTime")]
        public DateTime SendTime { get; set; }
    }

    public class Sender
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("isBot")]
        public bool IsBot { get; set; }
    }

}
