﻿using System;
using Newtonsoft.Json;

namespace Botsta.BotClient.Dto
{
    public class RefreshTokenResponse
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("hasError")]
        public bool HasError { get; set; }

        [JsonProperty("errorCode")]
        public string ErrorCode { get; set; }

        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }
    }
}
