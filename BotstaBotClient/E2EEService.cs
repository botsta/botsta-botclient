﻿using System;
using System.Security.Cryptography;
using System.Text;
using NSec.Cryptography;

namespace Botsta.BotClient
{
    public class E2EEService : IDisposable
    {
        private Key _key;
        private X25519 _x25519Algorithm = X25519.X25519;
        private Aes256Gcm _aesAlgorithm = Aes256Gcm.Aes256Gcm;

        public E2EEService()
        {
            _key = Key.Create(_x25519Algorithm);
        }

        public byte[] PublicKey => _key.PublicKey.Export(KeyBlobFormat.RawPublicKey);
       
        public string PublicKeyHex  => Convert.ToHexString(PublicKey);

        //public string EncryptMessage(string message, byte[] publicKeyPracticant)
        //{
        //    var secretKey = GetSecretKey(publicKeyPracticant);

        //    using AesGcm aes = new AesGcm(secretKey);
        //    aes.Encrypt();
        //}

        public string DecryptMessage(string encryptedMessage, byte[] pubKeyPracticant)
        {
            var (cipherText, nonce, mac) = SplitDecryptedMessage(encryptedMessage);

            using var sharedSecret = GetSharedSecret(pubKeyPracticant);

            using var derivedkey = KeyDerivationAlgorithm.HkdfSha512.DeriveKey(sharedSecret,
                ReadOnlySpan<byte>.Empty,
                ReadOnlySpan<byte>.Empty,
                _aesAlgorithm);

            byte[] decryptedMessage;
            if (!_aesAlgorithm.Decrypt(derivedkey, new Nonce(nonce, _aesAlgorithm.NonceSize), mac, cipherText, out decryptedMessage))
            {
                throw new Exception("Error decrypting message");
            }

            return Encoding.UTF8.GetString(decryptedMessage);
            
        }

        private (byte[] cipherText, byte[] nonce, byte[] mac) SplitDecryptedMessage(string encryptedMessage)
        {
            var msgParts = encryptedMessage.Split('.'); // cypherText.nonce.mac
            var cipherText = msgParts[0];
            var nonce = msgParts[1];
            var mac = msgParts[2];


            return (
                Encoding.UTF8.GetBytes(cipherText),
                Encoding.UTF8.GetBytes(nonce),
                Encoding.UTF8.GetBytes(mac)
                );
        }

        private SharedSecret GetSharedSecret(byte[] pubKeyPracticant)
        {
            var pubKey = NSec.Cryptography.PublicKey.Import(_x25519Algorithm, pubKeyPracticant, KeyBlobFormat.PkixPublicKeyText);
            var sharedSecret = _x25519Algorithm.Agree(_key, pubKey);
            return sharedSecret;
        }

        public void Dispose()
        {
            _key.Dispose();
        }
    }
}
