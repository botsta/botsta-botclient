﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GraphQL;

namespace Botsta.BotClient.Dto
{
    public class MessageBuilder : List<MessagePart>
    {
        public MessageBuilder AddText(string text)
        {
            if (!string.IsNullOrWhiteSpace(text))
            {
                Add(new MessagePart
                {
                    Text = text.Trim(),
                });
            }

            return this;
        }

        public MessageBuilder AddTitle(string title)
        {
            if (!string.IsNullOrWhiteSpace(title))
            {
                Add(new MessagePart
                {
                    Title = title.Trim(),
                });
            }

            return this;
        }

        public MessageBuilder AddSubtitle(string subtitle)
        {
            if (!string.IsNullOrWhiteSpace(subtitle))
            {
                Add(new MessagePart
                {
                    Subtitle = subtitle.Trim(),
                });
            }

            return this;
        }

        public MessageBuilder AddButton(MessageButton button)
        {
            if (button != null)
            {
                Add(new MessagePart
                {
                    Button = button,
                });
            }

            return this;
        }

        public MessageBuilder AddPostbackButton(string label, string postback)
        {
            if (!string.IsNullOrWhiteSpace(label) && !string.IsNullOrWhiteSpace(postback))
            {
                Add(new MessagePart
                {
                    Button = new MessageButton
                    {
                        Label = label.Trim(),
                        Postback = postback.Trim()
                    },
                });
            }

            return this;
        }

        public MessageBuilder AddUrlButton(string label, string url)
        {
            if (!string.IsNullOrWhiteSpace(label) && !string.IsNullOrWhiteSpace(url) && Uri.IsWellFormedUriString(url, UriKind.Absolute))
            {
                Add(new MessagePart
                {
                    Button = new MessageButton
                    {
                        Label = label.Trim(),
                        Url = url
                    },
                });
            }

            return this;
        }

        public async Task<GraphQLResponse<MessageResponse>> SubmittMessageAsync(BotstaBotClient client, Guid chatroomId)
        {
            return await client.SubmittMessageAsync(chatroomId, this);
        }

        public async Task<GraphQLResponse<MessageResponse>> SubmittMessageAsync(BotstaBotClient client, MessageResponse messageResponse)
        {
            return await client.SubmittMessageAsync(messageResponse.Message.ChatroomId, this);
        }
    }
}
